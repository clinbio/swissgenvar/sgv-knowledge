[![pipeline status](https://gitlab.sib.swiss/clinbio/swissgenvar/sgv-knowledge/badges/master/pipeline.svg)](https://gitlab.sib.swiss/clinbio/swissgenvar/sgv-knowledge/-/commits/master)

---

# SwissGenVar Knowledge Base website

Learn more about SwissGenVar Project at https://pages.sib.swiss/project/swissgenvar-doc

---
