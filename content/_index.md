---
title: Introduction
type: docs
---

## About this document

The SwissGenVar documentation is dedicated to all members of the project (clinicians, biologists and developers), but also to anyone interested in the project, in order to get a better understanding of the workflows, the ontologies and the ttool development.

This documentation is a **living document** that evolves over time, so if you notice that anything is missing or could be improved, please feel free to [contact us](mailto:dillenn.terumalai@sib.swiss).

