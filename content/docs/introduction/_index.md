---
title: Introduction
type: docs
weight: 1
---

# Introduction

This documentation intends to explain how the SwissGenVar platform has been developed and should be used.

--------

## About this document

SwissGenVar documentation is dedicated to all members of the project (clinicians, biologists and developers), in order to get a better understanding of the workflows, the ontologies used and the technical aspects.

{{< hint info >}}
This documentation is a **living document** that changes over time. If you see anything missing or that could be improved, feel free to [contact us](mailto:dillenn.terumalai@sib.swiss)!
{{< /hint >}}
