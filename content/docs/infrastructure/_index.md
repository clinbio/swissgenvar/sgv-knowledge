---
weight: 3
title: "Infrastructure"
---


This section gives an overview of the SwissGenVar infrastructure.

# Data management principles
In SwissGenVar, genetic data (whole exome or whole genome) are collected using the SPHN SETT transfer tool and are stored and accessed according to BioMedIT access and security standards (user identification with SWITCH edu-ID and 2-factor authentication). These genetic data are going through a technical check to ensure a minimal quality and that all compulsory fields (as already agreed) are present and correct before being loaded.
For the patients' phenotypic information, SwissGenVar currently only allows a manual entry via its web interface, using controlled vocabularies that were agreed upon during the SwissGenVar SPHN implementation project.
Data are then made visible to all Consortium members and the data providers are allowed to modify or complement their own data at any time.

# Data hosting and security
The SwissGenVar application (database, frontend, backend) and data are hosted on the SENSA BioMedIT node in Lausanne, on Weka for the sensitive information (native encryption). It therefore complies with the SPHN/BioMedIT Information Security Policy. Data transfers are ensured by the SPHN sett data transfer tool, which encrypts, securely transfers and decrypts data. Users' identity and access is managed with Keycloak, which has been setup with the support of the DCC. Two factor authentication is enforced, and data accesses are logged.

Access rights have been defined in the Data Transfer and Use Agreement (DTUA), and consist of 3 possible levels:
- **Public Access**: the SwissGenVar Platform public instance will be freely accessible by anyone without registration (limited data available).
- **Restricted Access**: upon approval of the Steering Board, data from the access-controlled instance (including personal data) may be made accessible to users belonging to a third-party group, (i.e. not member of the Consortium) if required for a specific research study and if authorized by the competent ethics committee.
- **Full Access:** all registered users belonging to a registered group (i.e. members of the Consortium) may access all data from the access-controlled instance (including personal data) in view-only mode, including data submitted by any other registered group. Registered users belonging to a registered group may in addition modify their own data or metadata.

# IT architecture

A specific IT architecture has been established with the BioMedIT experts, to balance the required level of security and the convenience of use for the project partners.
     

![Data flows](/Infrastructure.png)
