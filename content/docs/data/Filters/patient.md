---
title: Patient related filters
weight: 1
---
# Patient related fields

Here is an exhaustive list of all the patient related filters. This list aims to help all the project members to understand the role and the structure of each filter.

------

## Clinical status change to

### Description

This field corresponds to the established clinical status change.

### Content

This filter proposes a list of custom terms:

* Affected
* Partially affected
* Potentially affected
* Not affected

### Data source

None

---

## Clinical status at last clinical assessment

### Description

This field corresponds to the clinical status at last clinical assessment of the patient.

### Content

This filter will propose a list of custom terms:

* Affected
* Partially affected
* Potentially affected
* Not affected

### Data source

None

---

## Age at onset

### Description

This field corresponds to the age of the patient at disease onset.

### Content

This filter proposes a range of numbers for the age at onset.

### Data source

-1 (prenatal), 0, 0.1, 0.2, ..., 100

---

## Ethnicity

### Description

This field corresponds to the ethnicity.

### Content

This filter propose the list of **GnomAD populations** terms that are already present in the database.

### Data source

**GnomAD populations**

---

## Submitting institution

### Description

This field corresponds to the submitting institution.

### Content

This filter proposes the list of institutions that are already present in the database.

### Data source

None

---

## Canton

### Description

This field corresponds to the canton of the patient.

### Content

This filter will propose a list of Swiss cantons already present in the database.

### Data source

**Swiss cantons**

---

## Diagnosis

### Description

This field corresponds to the diagnosis of the patient.

### Content

This filter proposes a list of **OMIM** terms that are already present in the database.

### Data source

**OMIM**

---

## Clinical indication

### Description

This field corresponds to the clinical indication of the patient.

### Content

This filter proposes a list of **HPO** terms that are already present in the database.

### Data source

**HPO**

---

## Index patient

### Description

This field specifies if the patient is the index patient or not.

### Content

This filter proposes the following list (no, unknown, yes)

---

## Clinical status at last clinical assessment

### Description

This field corresponds to the clinical gender of the patient.

### Content

This filter proposes a list of custom terms:

* Ambiguous
* Female
* Male
* Not available
* Transgender

### Data source

None
