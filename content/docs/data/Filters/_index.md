---
weight: 5
---

# Filters

## Purpose

Filters act as criteria to refine searches and therefore, allow the user to find more easily the patient(s) or the variant(s) which he/she is looking for. A filter can be of different types (listed below) and use a predetermined list of terms or values. Some filters allow the user to select a list of terms, a range or a yes/no value.

## Description

### Classic filter

The classic filter is a list of a predetermined terms that can be selected. Either  none (default state), one term or multiple terms can be selected.

### Range filter

The range filter is used to narrow the range of a criteria, such as age for example. The user can type directly the values that he/she wants to use or use the circles to pick the desired values.

### Location filter

The location filter is a complex form to fill, to specify the location of a variant. It is divided into three parts: reference genome, chromosome and start-end positions.
