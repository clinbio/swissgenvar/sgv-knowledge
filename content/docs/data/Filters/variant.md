---
title: Variant related filters
weight: 1
---
# Variant related fields

Here is the list of all the available variant-related filters that can be used to query SwissGenVar.

------

## Clinical significance

### Description
This field corresponds to the clinical significance of the variant.

### Content
The following values can be used to filter:
- Pathogenic
- Likely pathogenic
- Uncertain significance
- Likely benign
- Benign

### Data source
Custom values adjusted from **ClinVar**.

---

## Causality

### Description
This field corresponds to the causality of the variant.

### Content
The following values can be used to filter:
- Causative
- Likely causative
- Probably not causative
- Not causative
- VUS
- Variant in a GUS

### Data source
Custom values.

---

## Variant type

### Description
This field corresponds to the type of the variant.

### Content
The following values from HGVS can be used to filter:

- CNV - amplification
- CNV - deletion
- CNV - insertion/duplication
- Complex
- Complex rearrangement
- Conversion
- Deletion
- Deletion-insertion
- Duplication
- Insertion
- Methylation/epigenetic change
- Repeat variation
- Structural variant
- Substitution

### Data source
Custom values.

---

## Variant effect

### Description
This field corresponds to the effect of the variant.

### Content
Here is the custom list of terms selected from VEP:

- Missense variant
- Nonsense
- Splicing variant
- Splice acceptor variant
- Splice donor variant
- Regulatory region
- Promoter region
- Inframe variant
- Inframe insertion
- Inframe deletion
- Intron variant
- Synonymous variant
- Stop lost
- Start lost
- Frameshift variant
- Upstream gene variant
- Downstream gene variant
- 5 prime UTR variant
- 3 prime UTR variant
- Exon deletion
- Exon duplication
- Contiguous gene deletion
- Contiguous gene duplication

### Data source
Partly coming from **VEP**.

---

## Gene locus type

### Description
This field corresponds to the gene locus type.

### Content
Here is the custom list of terms selected from HGNC:

- Protein coding gene
- Non-coding RNA gene
    - long non-coding RNA
    - microRNA
    - ribosomal RNA
    - transfer RNA
    - small nuclear RNA
    - small nucleolar RNA
    - other
- Locus subjected to imprinting

### Data source

Partly coming from **HGNC**

---

## Gene HUGO symbol

### Description
This field corresponds to the gene HUGO symbol.

### Content
This filter proposes a list of gene **HUGO** terms that are already present in the database.

### Data source
**HUGO**

---

## Location

### Description
This field corresponds to the location of the variant on a chromosome.

### Content
This filter proposes a list of genomes references (GrCh37 and GrCh38)and allows to enter a chromosome and genomic positions (start and end).

### Data source
None
