---
title: Patient query results
weight: 1
---
# Patient Query Results

Here is an exhaustive list of all the fields of the patient query results table.

------

# Patient results table

## Overview

### Description

The results table list all the patients matching the selected variants filtering criteria. They display only selected comparable information between the patients. If a user clicks on a row, he will be redirected to the detailed page of the patient which displays more information about the patient.

When no filter is used, the table lists ALL the patients prensent in the database. Filters act as a way to narrow the search and keep only the patients of interest.

If one or multiple filters are selected, the content of the table gets updated to display only the matching patients.

### Structure

Each row of the table corresponds to a patient matching the search.

---

## Fields

The list of all fields is presented below. More detailed information can be found when you go to the detailed page of a specific patient.

### Patient ID

#### Sortable

By default, the patient ID is used as the criteria to sort the patients. By clicking on the column name, the user can sort the patients by ascending or descending order using the patient identifier.

#### Description

The patient ID refers to an internal SwissGenVar specific unique identifier that is generated when the patient is created in the system.

#### Value

The patient ID is a simple integer.

---

### Submitting institution

#### Sortable

By clicking on the column name, the user can sort the patients by either ascending or descending order using the submitting instiution name.

#### Description

The institution which submitted the patient

#### Value

The value is the shortname of each submitting institutions

---

### Age

#### Sortable

By clicking on the column name, the user can sort the patients either by ascending or descending order using the age value.

#### Description

The age of the patient

#### Value

The value will numeric

---

### Clinical gender

#### Description

The clinical gender of the patient

#### Value

There are five possibles values, and each value is represented by an icon which can be hovered to display the value itself.

---

### Chromosomal sex

#### Description

The chromosomal sex of the patient

#### Value

The value displays the number of non-sexual chromosomes and the sexual chromosome (example: 46XY)

---

### Ethnicity

#### Description

The ethnicity of the patient

#### Value

The ethnicity is displayed as a coloured label to easily identify the different ethnicity groups

---

### Index patient

#### Description

Defines if the patient is the proband or not

#### Value

Displays a coloured icon which can be hovered to see its value (Yes, No or Unknown)

---

### Diagnosis

#### Description

The diagnosis of the patient

#### Value

Displays the Diagnosis (OMIM) as a black label with a check icon if the diagnosis is established. When clicking on one, it opens the corresponding **OMIM** page.

---

### Phenotypes

#### Description

The phenotypes of the patient

#### Value

Displays a list of labels corresponding of phenotypes of the patient. When clicking on one, it opens thecorresponding **HPO** page.
