---
weight: 5
---

# Result tables

## Purpose

When doing a query in SwissGenVar, results are mainly returned in tables. Tables generally are an easy mean to display summarized informations, to quickly compare rows between them and select the ones which are interesting. The various interface query result tables through the application are described thereafter.
