---
title: Variant query results
weight: 1
---
# Variant Query Results

Here is an exhaustive list of all the fields of the patient query results table.

------

# Variant results table

## Overview

### Description

The results table lists all the variants matching the selected criteria using filters. It displays only partial comparable information between the variants. If a user clicks on a row, he/she will be redirected to the detailed page of the variant which displays more information about it.

When no filter is used, the table automatically lists all the variants (of interest) present in the database. Filters act as a way to narrow the search and keep only the variants of interest.

If one or multiple filters are selected, the content of the table gets updated to display only the matching patients.

### Structure

Each row of the table corresponds to a variant matching the search

---

## Fields

All the fields are listed below. Their goal is to give the user a quick list of comparable information between the patients. More detailed information can be found on the detailed page of a specific patient.

### Variant type

#### Sortable

By clicking on the column name, the user sort the variant by ascending or descending order using the variant type.

#### Description

The variant type refers to specific type of the variant.

#### Value

Displays the variant type using coloured labels

---

### Genomic position

#### Sortable

By clicking on the column name, the user can sort the patient by ascending or descending order using the genomic position.

#### Description

The genomic position of the variant.

#### Value

The value is displayed using the following format: CHROM_NUMBER:START_POS-END_POS

---

### Gene name

#### Description

The gene name of the patient

#### Value

The gene HUGO symbol of the variant (may be multiple values)

---

### Cytogenetic location

#### Description

The cytogenetic location of the variant

#### Value

The value is displayed using the following format: CHROM_NUMBERq/pCYTOGENETIC_BAND

---

### ISCN name

#### Description

The ISCN name for inversions and translocations (structural variants)

#### Value

The value is displayed using the ISCN format ([source](https://en.wikipedia.org/wiki/International_System_for_Human_Cytogenetic_Nomenclature#:~:text=The%20International%20System%20for%20Human,human%20chromosome%20and%20chromosome%20abnormalities.))

---

### HGVS

#### Description

The HGVS identifiers

#### Value

The value is displayed using the HGVS format ([source](http://varnomen.hgvs.org/))

---

### Variant effect

#### Description

The effect of the variant

#### Value

The value can be one term from this list:
* Missense variant
* Stop gained
* Stop lost
* Start lost
* Frameshift variant
* Inframe variant
* Inframe insertion
* Inframe deletion
* Splicing variant
* Splice acceptor variant
* Splice donor variant
* Intron variant
* Synonymous variant
* Upstream gene variant
* Downstream gene variant
* 5 prime UTR variant
* 3 prime UTR variant
---
