---
weight: 4
title: "Data"
---

# Data

This section is intended to describe the SwissGenVar database structure, the catalogs and ontologies used, as well as the filters defined by the project members.
