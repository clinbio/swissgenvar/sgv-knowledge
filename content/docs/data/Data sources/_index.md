---
title: Data sources
weight: 1
---

SwissGenVar has been designed with a strong focus on interoperability with other SPHN projects and beyond. We therefore sticked to well-established standards, and in particular to the SPHN Interoperability Data Standard and Tool Collection guidelines whenever available.
In most cases, we used existing ontologies (see Catalogs and data sources), but for some fields we had to define common catalogs, that are a consensus between the practices at the different project's institutions (see Internal catalogs).

# Catalogs and data sources
The various catalogs and data sources are listed in the table below:

| Information | Data source | Obtained from | Full name |  Description | SPHN standard |
| ------ | ------ | ------ | ------ | ------ | ------ |
| Causality | OMIM | morbidmap.txt 	 | Online Mendelian Inheritance in Man | The genes causality will be retrieved from OMIM | |
| Clinical indication | HPO | | Human Phenotype Ontology | Standardized vocabulary of phenotypic abnormalities encountered in human disease | x |
| Clinical significance | ClinVar | VEP | | The ClinVar pathogenicity, using the 5 ACMG levels, will be displayed | |
| Diagnosis| OMIM | | Online Mendelian Inheritance in Man | | |
| Ethnicity | GnomAD | | | African/African-American, Amish, Latino/Admixed American, Ashkenazi Jewish, East Asian, Finnish, Non-Finnish European, Middle Eastern, South Asian, Other | |
| Frequency | GnomAD | VEP | | The minor allele frequency will come from GnomAD | |
| Gene name | HUGO | VEP | HUman Genome Organisation | Genes will be uniquely named according to the HUGO Gene Nomenclature | x |
| LRG sequence | LRG | EBI REST API (id) | Locus Reference Genomic | A Locus Reference Genomic (LRG) record contains stable reference sequences that are used for reporting sequence variants with clinical implications.  Documentation available on https://www.lrg-sequence.org/web-service/ and terms of use are readable on https://www.ebi.ac.uk/about/terms-of-use/  Example request : https://www.ebi.ac.uk/ebisearch/ws/rest/lrg?query=NM_004972.3&format=json | |
| Phenotype | HPO |	HPO API () | Human Phenotype Ontology | Standardized vocabulary of phenotypic abnormalities encountered in human disease | x |
| Transcripts | RefSeq, Ensembl | VEP | |Transcripts from both Refseq and Ensembl will be referenced | |
| Variant description | HGVS | VEP | Human Genome Variation Society | This nomenclature will used for the description of sequence variants (namely HGVSg., HGVSc. and HGVSp.). It will be complemented by ICSN (International System for Human Cytogenetic Nomenclature) for large genomic aberrations | x |

OMIM API: https://www.omim.org/help/api#1_2 (register to get a license: https://omim.org/api, but no answer so far)
HPO API: https://hpo.jax.org/webjars/swagger-ui/3.20.9/index.html?url=/api/hpo/docs
EBI REST API: https://www.lrg-sequence.org/web-service/


# Internal catalogs
| Information | Possible values | Description |
| ------ | ------ | ------ |
| Aneuploidies | Yes; No | The ISCN field should be close to this one  |
| Canton | List of Swiss cantons, plus "Non-Swiss" | |
| Chromosomal sex | XX; XY; Other | |
| Clinical gender | Male, Female, Ambiguous, Transgender | |
| Karyotypic sex | 45X, 46XX, 46XY, 47XXY, 47XYY, 47XXX | This field should be an expandable list, and its content is conditional to the value of "Chromosomal sex" |
| Clinical status | Affected; Partially affected; Potentially affected; Not affected | |
| Co-occurences | Yes; No | |
| Collection method | Case-control; Clinical testing; Reference population; Research; Other; Unknown | |
| Consent | Yes; No | |
| Detection method | Sequencing; Fragment analysis; Southern Blot; Conventional cytogenetics; FISH (IFISH or MFISH); Array (Oligo or SNP); qRTPCR; MLPA; NGS-based CNV detection (Panel/WES/WGS); Other; Not performed | |
| Gene locus type | Protein coding gene; Non-coding RNA gene; Long non-coding RNA; MicroRNA; Ribosomal RNA; Transfer RNA; Small nuclear RNA; Small nucleolar RNA: Other; Locus subjected to imprinting | Partly coming from HGNC |
| Index patient | Yes; No | |
| Inheritace of the disease | AD - Autosomal dominant, AR - Autosomal recessive, PD - Pseudoautosomal dominant, PR - Pseudoautosomal recessive, DD - Digenic dominant, DR - Digenic recessive, IC - Isolated cases, ICB - Inherited chromosomal imbalance, Mi - Mitochondrial, Mu - Multifactorial, SMo - Somatic mosaicism, SMu - Somatic mutation, XL - X-linked, XLD - X-linked dominant, XLR - X-linked recessive, YL - Y-linked | From OMIM |
| Inheritance of the variant | De novo constitutive; De novo mosaic; Paternally inherited, constitutive in father; Paternally inherited, mosaic in father; Maternally inherited, constitutive in mother; Maternally inherited, mosaic in mother; Biparental; Imbalance arising from a balanced parental rearrangement; Inherited mosaic; Unknown| |
| Locus subjected to imprinting	| Yes; No; Unknown | |
| Submitting institution | One acronym per partner institution | |
| Variant effect | Missense variant; Nonsense variant; Stop loss; Start loss; Intron variant; Frameshift variant; Inframe insertion; Inframe deletion; Splicing variant;   Synonymous variant; Regulatory variant; Non-coding transcript variant; Extension: Repeat expansion | Not compulsory; Intron variant stands for not splicing variants only, the rule to define splicing variants will have to be defined |
| Variant location | Upstream gene; Downstream gene; 5 prime UTR; 3 prime UTR; Promoter region; Intronic region; Coding region; Splicing region; Regulatory region; Intergenic region | Not compulsory |
| Variant type | Substitution; Deletion; Insertion; Duplication; Inversion; Conversion; Deletion-insertion; Repeat variation; Complex; Methylation/epigenetic change; CNV-deletion; CNV-duplication; CNV-amplfication; CNV-complex rearrangement; CNV-insertion; CNV-insertion/duplication; SV-translocation; SV-inversion; Aneuploidy | Adapted from HGVS |
| Variant zygosity | Heterozygous; Homozygous; Hemizygous; Mitochondrial heteroplasmy; Mitochondrial homoplasmy; Unknown; Mosaic; Chimeric; Ambiguous | Add the number of copies for CNVs |

# Genomic data format
VCF (Variant Call Format) format are used to upload genomic data into SwissGenVar.

# Computational predictions
Some computational predictions will be retrieved from the following sources:
- SIFT, Polyphen: currently obtained from VEP
- MutationTaster: no API, but provide on request a Perl script than can launch queries from a VCF (http://www.mutationtaster.org/info/automation.html) (not implemented yet)
- CADD if possible: CADD score can be freely downloaded from https://cadd.gs.washington.edu/download for non-commercial use (not implemented yet)
- Revel if popssible (not implemented yet)
- Eventually DANN for WGS (not implemented yet)

# Links to external data sources
SwissGenVar enables a direct link to the following well-known public data sources:
- Link to HGMD  (not implemented yet)
- Link to ClinVar
- Link to dbSNP
- Link to SVIP-O
- Link to Decipher to be evaluated (not implemented yet)
- Link to LOVD to be evaluated (not implemented yet)
