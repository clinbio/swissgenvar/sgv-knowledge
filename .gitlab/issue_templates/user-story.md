## Summary

(Summarize the user story. Example: As Max, I want to invite my friends, so we can enjoy this service together. - this is very important)

## Who

(Describe which type of user told this story - this is very important)

## When

(Describe when this story was told)

## Possible solution

(If possible, give hints about what should be used or done)